/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_source.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/27 00:10:06 by azhadan           #+#    #+#             */
/*   Updated: 2023/04/28 19:11:05 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putchar_fd(char c, int fd, int *len)
{
	write(fd, &c, 1);
	*len = *len + 1;
}

void	ft_putnbr_fd(int n, int fd, int *len)
{
	long	nb;

	nb = n;
	if (nb < 0)
	{
		ft_putchar_fd('-', fd, len);
		nb = -nb;
	}
	if (nb >= 10)
		ft_putnbr_fd(nb / 10, fd, len);
	ft_putchar_fd((nb % 10) + '0', fd, len);
}

void	ft_putstr_fd(char *s, int fd, int *len)
{
	int	i;

	i = 0;
	if (!s)
	{
		ft_putstr_fd("(null)", 1, len);
		return ;
	}
	while (s[i])
	{
		ft_putchar_fd(s[i], fd, len);
		i++;
	}
}

void	print_ptr_hex(unsigned long int ptr, int *len, int check)
{
	char	*hex;

	hex = "0123456789abcdef";
	if (!ptr)
	{
		ft_putstr_fd("(nil)", 1, len);
		return ;
	}
	if (check == 0)
		ft_putstr_fd("0x", 1, len);
	if (ptr >= 16)
		print_ptr_hex(ptr / 16, len, 1);
	ft_putchar_fd(hex[ptr % 16], 1, len);
}

void	ft_unsigned_putnbr(unsigned int num, int *len)
{
	if (num >= 10)
		ft_unsigned_putnbr(num / 10, len);
	ft_putchar_fd((num % 10) + '0', 1, len);
}
