/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/26 13:17:16 by azhadan           #+#    #+#             */
/*   Updated: 2023/04/28 18:38:32 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdio.h>
# include <string.h>
# include <unistd.h>

int		ft_printf(const char *str, ...);
void	ft_putnbr_fd(int n, int fd, int *len);
//in ft_source
void	ft_putchar_fd(char c, int fd, int *len);
//in ft_source
void	ft_putstr_fd(char *s, int fd, int *len);
//in ft_source
void	print_ptr_hex(unsigned long int ptr, int *len, int check);
//in ft_source
void	ft_unsigned_putnbr(unsigned int num, int *len);
//in ft_source
void	ft_puthex(unsigned int num, int *len, int up_or_low);
//in ft_second_source
#endif
