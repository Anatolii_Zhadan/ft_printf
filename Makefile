
FILESC = ft_printf.c ft_source.c ft_second_source.c

FILESO = $(FILESC:.c=.o)

CFLAGS = -Wall -Wextra -Werror

NAME = libftprintf.a

all: $(NAME)

$(NAME) : $(FILESO)
	ar -rcs $(NAME) $(FILESO)

clean:
		rm -f $(FILESO)

fclean: clean
		rm -f $(NAME)

re: fclean $(NAME)

.PHONY: all clean fclean re
